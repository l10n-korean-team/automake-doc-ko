This is automake-ko.info, produced by makeinfo version 4.0b from
automake-ko.texi.

INFO-DIR-SECTION GNU admin
START-INFO-DIR-ENTRY
* automake-ko: (automake-ko).    Makefile.in 파일 만들기
END-INFO-DIR-ENTRY

INFO-DIR-SECTION Individual utilities
START-INFO-DIR-ENTRY
* aclocal-ko: (automake-ko)aclocal 실행.     aclocal.m4 만들어 내기
END-INFO-DIR-ENTRY

   This file documents GNU automake 1.4-p4

   Copyright (C) 1995, 96, 97, 98 Free Software Foundation, Inc.

   Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided that
the entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Foundation.


File: automake-ko.info,  Node: Top,  Next: 소개,  Prev: (dir),  Up: (dir)

GNU Automake
************

   이 파일은 GNU Automake 패키지에 관한 문서이다.  GNU Automake는
틀(template) 파일에서부터 GNU 표준에 맞는 메이크파일(makefile)을 만들어
내는 도구이다.  이 판은 버전 1.4-p4에 해당하는 문서를 담고 있다.

* Menu:

* 소개::
* 일반::
* 예제::
* Automake 실행::
* configure::
* 최상위 레벨::
* 프로그램과 라이브러리::
* 그외 object::
* 그외 GNU Tool::
* 문서::
* 설치::
* Clean::
* 배포::
* 테스트::
* Options::
* Miscellaneous::
* 조건::
* Gnits::
* Cygnus::
* 확장::
* 배포하기::
* 미래::
* 색인::


File: automake-ko.info,  Node: 소개,  Next: 일반,  Prev: Top,  Up: Top

소개
****

   Automake는 `Makefile.am'이라는 파일에서부터 `Makefile.in'을 자동으로
만들어 내는 도구이다.  `Makefile.am'은 기본적으로 `make' 매크로
정의(macro definition)를 죽 써 놓은 파일이다 (가끔 룰(rule)도
나타난다).  여기서 만들어진 `Makefile.in'은 GNU 메이크파일 표준(the GNU
Makefile Standards)에 맞는다.

   GNU 메이크파일 표준(the GNU Makefile Standards)(*note Makefile
Conventions: (standards)Makefile Conventions.)은 길고, 복잡한 문서이며,
또 곧 변경될 예정이다.  Automake의 목표는 각 GNU 관리자의 등에서
메이크파일을 관리하는 부담을 덜어 주는 것이다 (그리고 그 부담을
Automake의 관리자에게 넘긴다).

   보통의 경우 Automake 입력 파일은 단순히 매크로 정의(macro
definition)를 연속해서 쓴 것이다.  이 입력 파일들이 처리되어 각각의
입력 파일당 한개씩의 `Makefile.in' 파일이 만들어진다.  일반적으로
프로젝트의 각 디렉토리마다 `Makefile.am'이 한 개씩 필요하다.

   Automake는 프로젝트를 제한하는 점이 몇가지 있다; 한가지 예로
Automake를 사용하는 프로젝트는 Autoconf(*note The Autoconf Manual:
(autoconf)Top.)를 사용한다고 가정되며, `configure.in'에 들어갈 수 있는
내용도 몇가지 제한이 있다.

   Automake로 `Makefile.in' 파일을 만들려면 `perl'이 필요하다.  하지만,
Automake로 만들어진 배포판은 완전히 GNU 표준에 들어 맞고, 빌드할 때
`perl'이 필요없다.

   Automake에 대한 제안이나 버그 보고는 <bug-automake.org>로 보내 주기
바란다.


File: automake-ko.info,  Node: 일반,  Next: 예제,  Prev: 소개,  Up: Top

일반적인 아이디어
*****************

   다음에서는 어떻게 Automake가 동작하는지 이해하는데 필요한 기초적인
아이디어들을 다룬다.

* Menu:

* 일반적인 동작::
* 깊이::
* 엄격성::
* 일관적::
* 규범화::


File: automake-ko.info,  Node: 일반적인 동작,  Next: 깊이,  Prev: 일반,  Up: 일반

일반적인 동작
=============

   Automake가 동작하면 `Makefile.am' 파일을 읽고, `Makefile.in' 파일을
만든다.  `Makefile.am'에 정의할 수 있는 매크로(macro)와
타겟(target(1))중에서 특정한 몇개는 Automake가 더욱 특별한 코드를
만들도록 알려 주는 역할을 한다; 예를 들어 `bin_PROGRAMS' 매크로를
정의하면 컴파일되고, 링크되야 하는 타겟을 `Makefile.in'에 만들어낼
것이다.

   `Makefile.am' 안의 매크로 정의와 타겟은, 앞으로 만들어 질
`Makefile.in' 파일에 그대로 복사된다.  그러므로 어떤 임의의 코드라도
생성되는 `Makefile.in' 파일에 집어넣을 수 있다.  예를 들어 Automake
배포판에는 비표준적인 `cvs-dist' 타겟이 들어있다.  이 타겟은 Automake
관리자가 소스 콘트롤 시스템에서 배포판을 만들때 이용한다.

   Automake는 GNU make만의 확장기능을 이해하지 못한다는 점에 유의하기
바란다.  `Makefile.am'에 그런 확장 기능을 사용하면 애러를 내거나,
괴상하게 동작할 것이다.

   Automake는 똑똑한 방법으로 주석문을 인접한 타겟과 매크로 정의에 모아
놓는다.

   보통 `Makefile.am'에 정의된 타겟이 `automake'가 자동으로 생성해 내는
같은 이름의 타겟보다 우선하고, 같은 이름이 있으면 재정의된다.  이것은
지원되는 기능중 하나이지만, 이 기능을 사용하지 않는 것이 좋다.
왜냐하면 이렇게 자동으로 생성되는 룰이 상세하게 잘 만들어져 있기
때문이다.

   마찬가지로, `Makefile.am'에 변수를 정의하면 일반적으로 `automake'가
자동으로 만들어 내는 변수의 정의를 무시하고 재정의한다.  이 기능은
타겟의 정의를 재정의하는 기능보다 더 유용할 때가 훨씬 많다.  하지만,
`automake'가 만들어 내는 많은 변수들은 내부적으로 사용된다고 가정하고
만들어 졌으므로, 훗날 그 이름이 바뀔 수도 있다는 것을 경고해 둔다.

   Automake가 변수의 정의를 검사할 때, Automake는 재귀적으로 그
변수정의에 또 다른 변수가 들어가 있는지 검사할 것이다.  그 예로,
Automake가 다음 부분에서 `foo_SOURCES'의 내용을 검사할 때,

     xs = a.c b.c
     foo_SOURCES = c.c $(xs)

   `foo_SOURCES'의 값으로 `a.c', `b.c', 그리고 `c.c' 파일을 사용하게 될
것이다.

   Automake의 주석문중에는 `Makefile.in'에 출력될 때 복사되지 _않는_
주석문도 있다; `##'로 시작하는 모든 줄은 Automake가 완전히 무시한다.

   `Makefile.am'의 첫번째 줄은 관례적으로 다음과 같이 한다:

     ## Process this file with automake to produce Makefile.in

   ---------- Footnotes ----------

   (1) 역자주: Makefile의 의존성(dependency)에서 콜론(:) 앞에 오는 것


File: automake-ko.info,  Node: 깊이,  Next: 엄격성,  Prev: 일반적인 동작,  Up: 일반

깊이
====

   `automake'는 세 가지 종류의 디렉토리 계층구조를 지원한다; `flat',
`shallow', 그리고 `deep'이다.

   "flat" 패키지는 모든 파일들이 한개의 디렉토리에 들어 있는
패키지이다.  정의가 이러하므로 flat 패키지의 `Makefile.am'는 `SUBDIRS'
매크로가 없다.  이러한 패키지의 한 예는 `termutils'이다.

   "deep" 패키지는 모든 소스 파일이 서브디렉토리에 있는 패키지이다; 맨
위 디렉로티는 주로 설정 파일들만 들어 있다.  GNU cpio는 이러한 패키지의
좋은 예이고, GNU `tar'도 그렇다.  deep 패키지에서 맨 위 디렉토리의
`Makefile.am'는 `SUBDIRS' 매크로가 있고, 반면 이 파일의 어떤
매크로에서도 빌드해야 할 파일을 정의하지 않을 것이다.

   "shallow" 패키지는 주요 소스가 맨 위 디렉토리에 있고, 다른 여러가지
부분(보통 라이브러리)이 서브디렉토리에 있는 패키지이다.  `automake'는
그러한 패키지이다.  (현재 `automake'를 사용하지 않긴 하지만, GNU
`make'도 그렇다.)


File: automake-ko.info,  Node: 엄격성,  Next: 일관적,  Prev: 깊이,  Up: 일반

엄격성
======

   Automake는 GNU 패키지 관리자가 쓰도록 한 것이지만, Automake를
사용하고 싶으면서, GNU의 관습 전부를 쓰고 싶지 않은 사람의 편의도
고려했다.

   이를 위해, Automake는 세가지 단계의 "엄격성(strictness)"를
지원한다--엄격함은 표준을 따르는지 아닌지 얼마나 강력하게 검사해야
하는지를 나타낸다.

   사용할 수 있는 엄격성 단계는:

`foreign'
     Automake는 제대로 동작을 하는데 꼭 필요한 것만 검사한다.  예를
     들어, GNU 표준에서는 `NEWS' 파일이 있어야 한다고 정해져 있지만, 이
     모드에서는 `NEWS' 파일이 필요없다.  `foreign'이라는 이름은
     Automake가 GNU 프로그램에 사용되기 위한 도구라는 사실에서부터
     나왔다 지어졌다; 이 느슨한 규칙은 기본 동작 모드가 아니다.

`gnu'
     Automake는--가능하면 많이--패키지가 GNU 표준을 따르는지 검사한다.
     이것이 기본 동작 모드이다.

`gnits'
     Automake는 아직-작성되지-않은 "Gnits 표준"을 따르는지 검사한다.
     Gnits는 GNU 표준에 기초하지만, 훨씬 더 자세하다.  GNITS 표준을
     만드는 데 참여하는 사람이 아니라면, Gnits 표준이 실제로 발표될
     때까지 이 옵션을 사용하지 않기를 권한다.

   엄격성 단계에 정확히 무엇이 포함되었는 지에 대한 정확한 정보는 *Note
Gnits::.


File: automake-ko.info,  Node: 일관적,  Next: 규범화,  Prev: 엄격성,  Up: 일반

일관적 명명법
=============

   Automake 매크로(여기서부터 _변수_라고 부르겠다)는 보통 "일관적인
명명법"을 따른다.  이 명명법을 통해 어떻게 프로그램(그리고 그외 다른
것으로부터 자동으로 만들어지는 오브젝트들)이 빌드되고, 어떻게
설치되는지 Automake가 더 쉽게 알 수 있다.  또 이 명명법을 이용해
`configure' 시에 무엇을 빌드해야 할지 결정하는 것까지도 지원한다.

   `make'를 실행할때, 어느 오브젝트를 빌드해야 할지 알아보는 데 쓰이는
변수가 몇 개 있다.  이 변수를 "주요(primary) 변수"라고 한다.  그 한가지
예로, 주요 변수인 `PROGRAMS'의 값은 컴파일되고 링크될 프로그램들의
리스트를 담고 있다.

   또 다른 종류의 변수들은 만들어진 오브젝트를 어디에 설치해야 할지
결정한다.  이 변수들의 이름은 주요 변수의 이름을 본따서 이름지어 졌지만,
어느 표준 디렉토리를 설치 디렉토리로 사용할지 나타내는 접두어를 갖고
있다.  이 표준 디렉토리들의 이름은 GNU 표준에 주어져 있다 (*note
Directory Variables: (standards)Directory Variables.).  `automake'는 이
리스트를 확장해서 `pkglibdir', `pkgincludedir', 그리고 `pkgdatadir'를
추가했다; 이 변수들은 `pkg'가 없는 버전과 같지만, 디렉토리에
`@PACKAGE@'가 뒤에 덧붙여진 이름이다.  예를 들어, `pkglibdir'는
`$(libdir)/@PACKAGE@'로 정의된다.

   각 주요 변수에 대하여, 주요 변수의 이름 앞에 `EXTRA_'를 붙인 변수가
한가지씩 추가되어 있다.  이 변수는 `configure'가 어떻게 결정하냐에
따라서 만들어 질 수도 있고 만들어 지지 않을 수도 있는 오브젝트를
나열하는 데 쓰인다.  어떤 경우에도 제대로 동작하는 `Makefile.in'을
만드려면, 만들어야 할 오브젝트의 전체 리스트를 Automake가 고정적으로
알고 있어야 하기 때문에 이 변수가 필요하다.

   그 한가지로, `cpio'는 `configure' 실행 시에 어떤 프로그램을 빌드할지
결정한다.  어떤 프로그램은 `bindir'에 설치할 것이고, 어떤 프로그램은
`sbindir'에 설치할 것이다:

     EXTRA_PROGRAMS = mt rmt
     bin_PROGRAMS = cpio pax
     sbin_PROGRAMS = @PROGRAMS@

   접두어가 없이 주요 변수를 정의하는 것은 (`PROGRAMS'처럼) 잘못된
명명법이다.

   공통적으로 들어 있는 `dir' 접미어는 변수 이름을 만드는 데 없앤다.
즉, `bin_PROGRAMS'으로 쓰지 `bindir_PROGRAMS'라고 쓰지는 않는다.

   각각의 디렉토리에 설치될 수 있는 오브젝트의 종류는 제한되어 있다.
만약 해당 디렉토리에 설치할 수 없는 오브젝트를 설치하려는 시도를 하면
Automake는 오류로 처리한다.  또 Automake는 디렉토리 이름을 잘못 쓴
경우를 찾아낼 것이다.

   때때로 표준 디렉토리들--Automake에 의해 변형된 디렉토리
이름이라도--로는 충분하지 않은 경우가 있다.  특히 더 명확하게 하기
위해, 오브젝트를 어떤 미리 정의된 디렉토리의 서브디렉토리에 설치하는
것이 유용할 때도 있다.  이것 때문에, Automake는 설치 가능한 디렉토리를
확장할 수 있게 해준다.  임의의 접두어(예로 `zar')도 같은 이름에 `dir'가
뒤에 붙여진 변수(예로 `zardir')를 정의하면 사용할 수 있다.

   예를 들어, Automake가 HTML을 지원할 때까지 HTML 문서를 설치할 때
다음과 같이 사용할 수 있다.

     htmldir = $(prefix)/html
     html_DATA = automake.html

   특별 접두어 `noinst'는 주어진 object들이 전혀 설치되지 말아야 한다는
뜻이다.

   특별 접두어 `check'는 주어진 object들이 `make check' 명령이 실행될
때까지는 만들어지지 말아야 한다는 뜻이다.

   사용 가능한 가능한 주요 변수의 이름은 `PROGRAMS', `LIBRARIES',
`LISP', `SCRIPTS', `DATA', `HEADERS', `MANS', 그리고 `TEXINFOS'이다.


File: automake-ko.info,  Node: 규범화,  Prev: 일관적,  Up: 일반

파생된 변수의 명명법
====================

   때때로 사용자가 쓰는 또 다른 문자열에서 Makefile 변수의 이름이
결정되는 경우가 있다.  그 한가지 예로, 프로그램의 이름은 Makefile의
매크로 이름으로 쓰여진다.  Automake는 이 문자열을 Makefile 변수규칙에
맞게 변형해서, Makefile 변수의 명명법대로 이름을 지을 필요가 없도록
해준다.  글자, 숫자, 그리고 밑줄을 제외한 이름의 모든 문자는 매크로를
만들때 밑줄로 바뀐다.  예를 들어, 만약 프로그램의 이름이
`sniff-glue'라면, 여기에서 나오는 변수의 이름은 `sniff_glue_SOURCES'이지
`sniff-glue_SOURCES'가 아니다.


File: automake-ko.info,  Node: 예제,  Next: Automake 실행,  Prev: 일반,  Up: Top

몇몇 예제 패키지
****************

* Menu:

* 완전::
* Hello::
* etags::


File: automake-ko.info,  Node: 완전,  Next: Hello,  Prev: 예제,  Up: 예제

간단한 예제, 시작부터 끝까지
============================

   당신의 머리가 소용돌이에서 소용돌이 사이로 떠다니게 하는 프로그램인,
`zardoz'의 작성을 끝냈다고 가정하자.  호환성 높은 구조를 위해
`autoconf'를 사용했지만 `Makefile.in' 파일들은 임기응변으로 만들었다.
이 파일들을 튼튼하게 하기 위해서, `automake'로 눈을 돌렸다.

   첫번째로 할 일은, `configure.in'에 `automake'가 필요로 하는 명령을
포함시키는 것이다.  가장 간단한 방법은 `AC_INIT' 바로 다음에
`AM_INIT_AUTOMAKE'를 추가하는 것이다.

     AM_INIT_AUTOMAKE(zardoz, 1.0)

   이 프로그램은 어떤 복잡한 요소도 없기 때문에 (예를 들어, `gettext'를
사용하지도 않고, 동적 라이브러리를 만들려고 하지도 않는다), 이 부분은
끝마쳤다.  아주 쉽다!

   이제 `configure'를 다시 만들어야 한다.  하지만 이 일을 하려면,
`autoconf'에게 당신이 사용중인 새로운 매크로가 어디있는지 알려줘야
한다.  가장 쉬운 방법은 `aclocal' 프로그램을 사용해서 `aclocal.m4'
파일을 만드는 것이다.  하지만 아직... 이 프로그램을 위해서 이미 몇개의
매크로를 어렵게 작성해서, 이미 `aclocal.m4' 파일을 가지고 있을 수도
있다.  `aclocal'에서는 손수 작성한 매크로를 `acinclude.m4'에 넣을 수
있다.  그래서, 단지 이름을 바꾸고 실행하면 된다:

     mv aclocal.m4 acinclude.m4
     aclocal
     autoconf

   이제 `zardoz'를 위한 `Makefile.am'을 작성해야 할 때다.  `zardoz'는
사용자 프로그램이기 때문에, 나머지 다른 사용자 프로그램이 있는
디렉토리에 설치하고 싶다.  또 `zardoz'는 Texinfo 문서도 가지고 있다.
`configure.in' 스크립트에서는 `AC_REPLACE_FUNCS'를 사용하기 때문에,
`@LIBOBJS@'와 링크해야 한다.  즉 다음과 같이 작성해야 한다:

     bin_PROGRAMS = zardoz
     zardoz_SOURCES = main.c head.c float.c vortex9.c gun.c
     zardoz_LDADD = @LIBOBJS@
     
     info_TEXINFOS = zardoz.texi

   이제 `automake --add-missing'을 실행하면, `Makefile.in'을 만들고,
필요한 몇몇 보조 파일들을 가져온다.  그리고 당신은 해냈다!


File: automake-ko.info,  Node: Hello,  Next: etags,  Prev: 완전,  Up: 예제

전통적 프로그램
===============

   GNU hello (ftp://ftp.gnu.org/pub/gnu/hello/hello-1.3.tar.gz)는 그
전통적인 간단함과 융통성때문에 유명하다.  이 절은 Automake가 GNU Hello
패키지에 어떻게 사용될 수 있는지 설명준다.  아래의 예는 가장 최신의 GNU
Hello에서 온 것이지만, 저작권에 관한 언급과 같이 메인테이너에게만 필요한
코드는 전부 뺐다.

   물론, GNU Hello는 소스가 두줄밖에 안 되는 전통적인 Hello보다 훨씬 더
기능이 많다.  GNU Hello는 국제화 되어 있고, 옵션을 처리하고, 매뉴얼과
테스트 모음이 들어 있다.  GNU Hello는 deep 패키지이다.

   여기에 GNU Hello의 `configure.in'이 있다:

     dnl Process this file with autoconf to produce a configure script.
     AC_INIT(src/hello.c)
     AM_INIT_AUTOMAKE(hello, 1.3.11)
     AM_CONFIG_HEADER(config.h)
     
     dnl Set of available languages.
     ALL_LINGUAS="de fr es ko nl no pl pt sl sv"
     
     dnl Checks for programs.
     AC_PROG_CC
     AC_ISC_POSIX
     
     dnl Checks for libraries.
     
     dnl Checks for header files.
     AC_STDC_HEADERS
     AC_HAVE_HEADERS(string.h fcntl.h sys/file.h sys/param.h)
     
     dnl Checks for library functions.
     AC_FUNC_ALLOCA
     
     dnl Check for st_blksize in struct stat
     AC_ST_BLKSIZE
     
     dnl internationalization macros
     AM_GNU_GETTEXT
     AC_OUTPUT([Makefile doc/Makefile intl/Makefile po/Makefile.in \
                src/Makefile tests/Makefile tests/hello],
        [chmod +x tests/hello])

   `AM_' 매크로들은 Automake가 (혹은 Gettext 라이브러리가) 제공하는
것이다; 나머지는 표준 Autoconf 매크로이다.

   가장 윗 디렉토리에 있는 `Makefile.am'은:

     EXTRA_DIST = BUGS ChangeLog.O
     SUBDIRS = doc intl po src tests

   보시다시피, 실제 작업은 서브디렉토리에서 일어난다.

   `po'와 `intl' 디렉토리는 `gettextize'를 써서 자동으로 만들어 진다:
이것에 대해서는 여기서 설명하지 않는다.

   `doc/Makefile.am'에서는:

     info_TEXINFOS = hello.texi
     hello_TEXINFOS = gpl.texi

   이것만 있으면 충분히 GNU Hello 매뉴얼을 만들고, 설치하고, 그리고
배포하는 데 충분하다.

   다음은 `tests/Makefile.am'이다:

     TESTS = hello
     EXTRA_DIST = hello.in testdata

   `hello' 스크립트는 `configure'가 만들어 낸다.  그리고 이 스크립트는
유일한 테스트케이스이다.  `make check'가 이 테스트를 실행할 것이다.

   실제로 모든 작업이 일어나는 `src/Makefile.am'을 보자:

     bin_PROGRAMS = hello
     hello_SOURCES = hello.c version.c getopt.c getopt1.c getopt.h system.h
     hello_LDADD = @INTLLIBS@ @ALLOCA@
     localedir = $(datadir)/locale
     INCLUDES = -I../intl -DLOCALEDIR=\"$(localedir)\"


File: automake-ko.info,  Node: etags,  Prev: Hello,  Up: 예제

Building etags and ctags
========================

   여기에 또다른, 트릭이 더 많이 사용된 예제가 있다.  이 예제에서는 같은
소스 파일(`etags.c')에서 어떻게 두개의 프로그램(`ctags'와 `etags')을
만들어 낼 수 있는지 알수 있다.  여기서 어려운 부분은 `etags.c'를 각각
컴파일할때 다른 `cpp' 옵션이 필요하다는 것이다.

     bin_PROGRAMS = etags ctags
     ctags_SOURCES =
     ctags_LDADD = ctags.o
     
     etags.o: etags.c
             $(COMPILE) -DETAGS_REGEXPS -c etags.c
     
     ctags.o: etags.c
             $(COMPILE) -DCTAGS -o ctags.o -c etags.c

   `ctags_SOURCES'는 값이 없는 변수로 정의된다는 것에 유의하자--이렇게
하면 이 변수의 값으로 어떤 내포적인 값이 사용되지 않는다.  하지만,
`etags.o'에서 `etags'를 만드는 것은 내포적인 규칙이다.

   링크 명령에 `ctags.o'가 들어가도록 하기 위해 `ctags_LDADD'가 쓰인다.
한편 `ctags_DEPENDENCIES'는 Automake로 만들어진다.

   만약 컴파일러가 `-c'와 `-o' 옵션을 동시에 받아들일 수 없다면, 위의
룰(rule)은 동작하지 않는다.  이걸 간단히 고치는 방법은 가짜 의존성을
하나 추가시키는 것이다 (병렬적으로 `make' 실행할때 문제를 피하기
위해서).

     etags.o: etags.c ctags.o
             $(COMPILE) -DETAGS_REGEXPS -c etags.c
     
     ctags.o: etags.c
             $(COMPILE) -DCTAGS -c etags.c && mv etags.o ctags.o

   또, 만약 ANSI C 문법을 없애는 기능을 사용한다면(*note ANSI::), 이
룰은 동작하지 않을 것이다; ANSI C 문법을 없애는 기능까지 지원하려면
조금 더 작업해야 한다:

     etags._o: etags._c ctags.o
             $(COMPILE) -DETAGS_REGEXPS -c etags.c
     
     ctags._o: etags._c
             $(COMPILE) -DCTAGS -c etags.c && mv etags._o ctags.o


File: automake-ko.info,  Node: Automake 실행,  Next: configure,  Prev: 예제,  Up: Top

`Makefile.in' 만들기
********************

   한 패키지 내의 모든 `Makefile.in' 파일들을 만드려고 할 때,
`automake' 프로그램을 맨 위 디렉토리에서 아무 인자 없이 실행한다.
`automake'는 자동으로 각각 알맞은 `Makefile.am'을 찾고
(`configure.in'을 검색해서; *note configure::) 대응되는 `Makefile.in'을
만들어 낸다.  `automake'는 패키지가 매우 간단하게 구성되어 있다고
가정한다; `automake'는 한개의 패키지에 오직 한개의 `configure.in'가 맨
위 디렉토리에 있다고 가정한다.  만약 패키지가 여러개의 `configure.in'을
갖고 있다면, `configure.in'이 들어 있는 각각의 디렉토리에서 `automake'를
실행해야 한다.

   옵션으로 `automake'에 인자를 줄 수 있다; `.am'를 그 인자 뒤에 붙인
결과가 입력 파일의 이름으로 사용된다.  보통 이 기능은 업데이트해야 하는
`Makefile.in'을 다시 만들 때 자동으로 사용된다.  경우 서브디렉토리의
`Makefile.in'을 다시 만들 때 쓰일 경우에도 `automake'는 언제나
프로젝트의 가장 위의 디렉토리에서 실행되어야 한다는 것에 유의하자.  그
이유는 `automake'가 `configure.in'을 읽기 때문이고, 어떤 경우에는
`Makefile.in'이 서브디렉토리에 있다는 사실을 이용해 `automake'가 다르게
동작하기 때문이다.

   `automake'에 다음 옵션을 줄 수 있다.

`-a'

`--add-missing'
     어떤 상황에서는 Automake에서 꼭 필요한 파일들이 있다; 한 가지 예로
     `configure.in'에서 `AC_CANONICAL_HOST'를 실행하려면 `config.guess'
     파일이 필요하다.  Automake는 이와 같은 파일들이 함께 배포된다; 이
     옵션은 (가능하다면) 빠진 파일들을 자동으로 패키지에 첨가시켜 준다.
     보통 Automake를 실행할 때 어떤 파일이 없다는 메세지를 내면, 이
     옵션을 사용해 본다.  기본적으로 Automake는 빠진 파일에 대해서
     심볼릭 링크를 만들어 놓는다; 단, `--copy' 옵션을 사용하면 그렇게
     하지 않는다.

`--amdir=DIR'
     Automake의 데이타 파일을 설치 디렉토리 대신에 DIR 디렉토리에서
     찾는다.  이 옵션은 보통 디버깅을 위해 사용된다.

`--build-dir=DIR'
     Automake에게 어디 빌드 디렉토리가 있는지 알려준다.  이 옵션은 `make
     dist'에서 `Makefile.in' 파일을 만들 때 이 파일에 의존성 정보를
     포함시킬 때 사용한다; 그 외에는 사용되면 안 된다.

`-c'

`--copy'
     `--add-missing' 옵션과 같이 사용하면, 설치할 파일들이 복사된다.
     기본적으로 심볼릭 링크를 만든다.

`--cygnus'
     생성된 `Makefile.in'이 GNU나 Gnits 규칙 대신에 Cygnus 규칙을
     따르도록 한다.  자세한 정보는 *Note Cygnus::.

`--foreign'
     전체 엄격성을 `foreign'로 맞춘다.  더 많은 정보는 *Note 엄격성::.

`--gnits'
     전체 엄격성을 `gnits'로 맞춘다.  더 많은 정보는 *Note Gnits::.

`--gnu'
     global striceness를 `gnu'로 맞춘다.  더 많은 정보는 *Note Gnits::.
     이 것이 기본 엄격성이다.

`--help'
     셸 명령의 옵션 요약을 출력하고 종료한다.

`-i'

`--include-deps'
     자동으로 만들어지는 의존성 정보(*note 의존성::)를 `Makefile.in'
     파일 안에 포함한다.  배포판을 만드는 경우에는 이렇게 한다; *Note
     배포:: 참조.

`--generate-deps'
     자동으로 만들어지는 의존성 정보를 (*note 의존성::) 모두 한 파일에
     모아 놓은 파일, `.dep_segment'를 만든다.  이 작업은 보통 배포판을
     만들 때 하게 된다; *Note 배포:: 참조.  이 기능은 `SMakefile'처럼
     여러 플랫폼을 위한 메이크파일을 관리해야 할 때 유용하다
     (`Makefile.DOS', 등).  이 옵션은 `--include-deps',
     `--srcdir-name', 그리고 `--build-dir' 옵션과 함께 사용해야 한다.
     이 옵션이 주어지면, 그 외의 동작은 전혀 하지 않는다는 점에
     유의한다.

`--no-force'
     보통 `automake'는 `configure.in'에 언급된 모든 `Makefile.in'
     파일들을 만든다.  이 옵션은 그 파일들의 의존성에 따라 갱신해야
     하는 `Makefile.in'만을 다시 만들도록 한다.

`-o DIR'

`--output-dir=DIR'
     생성된 `Makefile.in'을 DIR 디렉토리에 넣는다.  보통 각
     `Makefile.in'은 해당 `Makefile.am'이 있는 디렉토리에 만들어 진다.
     이 옵션은 배포판을 만들 때 사용된다.

`--srcdir-name=DIR'
     Automake에게 현재 빌드와 관련된 소스 디렉토리의 이름을 알려준다.
     이 옵션은 `make dist'에 의해 만들어진 `Makefile.in'에 의존성
     정보를 포함시킬 때 사용한다; 그 외에는 사용되면 안 된다.

`-v'

`--verbose'
     Automake에서 입력을 읽거나 Automake가 만들어 내는 파일들에 대한
     정보를 출력하도록 한다.

`--version'
     Automake의 버전 번호를 출력하고 종료한다.


File: automake-ko.info,  Node: configure,  Next: 최상위 레벨,  Prev: Automake 실행,  Up: Top

`configure.in'를 검색하기
*************************

   Automake는 패키지의 `configure.in'을 검색해서 패키지에 어떤 정보가
있는지 결정한다.  몇개의 `autoconf' 매크로가 필요하고, 몇개의 변수가
`configure.in'에 정의되어야 한다.  Automake는 `configure.in' 안의
정보를 사용해서 더욱 알맞게 출력물을 낼 것이다.

   Automake는 메인테이너를 좀더 쉽게 해주는 `autoconf' 매크로를 같이
포함한다.  이 매크로들은 `aclocal' 프로그램을 써서 자동으로
`aclocal.m4'에 추가된다.

* Menu:

* 필요한 것::
* 옵션::
* aclocal 실행::
* 매크로::
* aclocal 확장::


File: automake-ko.info,  Node: 필요한 것,  Next: 옵션,  Prev: configure,  Up: configure

Configuration에 필요한 것
=========================

   Automake에서 필요한 기본적인 것을 만족시키는 가장 간단한 방법은
`AM_INIT_AUTOMAKE' 매크로를 사용하는 것이다 (FIXME: xref).  하지만, 더
좋아한다면, 필요한 것을 하나씩 손으로 해 나갈 수 있다.

   * `PACKAGE'와 `VERSION' 변수를 `AC_SUBST'와 함께 정의한다.
     `PACKAGE'는 배포하려고 묶을 때 나타나는 패키지의 이름이 되야 한다.
     예를 들어, Automake는 `PACKAGE'를 `automake'로 정의하였다.
     `VERSION'은 개발된 것을 release할 때 버전 번호가 되야 한다.
     `configure.in'이 패키지에서 버전 번호가 정의된 유일한 곳으로
     하라고 추천한다: 이렇게 하면 release가 간편해 진다.

     Automake는 `PACKAGE'나 `VERSION'에 대해서 `Gnits' 모드에 있을 때를
     제외하고는 어떤 해석도 하지 않는다.

   * 프로그램이나 스크립트를 한개라도 설치한다면 `AC_ARG_PROGRAM'
     매크로를 사용한다.

   * 패키지가 flat이 아니라면 `AC_PROG_MAKE_SET'을 사용한다.

   * 컴파일 환경이 제대로 되었는지 확인하려면, `AM_SANITY_CHECK'를 쓴다.

   * 패키지에 스크립트가(*note 스크립트::) 한개라도 설치되었다면
     `AM_PROG_INSTALL'을 사용한다.  그렇지 않으면, `AC_PROG_INSTALL'.

   * `aclocal', `autoconf', `automake', `autoheader', 그리고
     `makeinfo'가 컴파일 환경에서 존재하는지 알아보려면,
     `AM_MISSING_PROG'를 사용한다.  다음과 같이 한다:
          missing_dir=`cd $ac_aux_dir && pwd`
          AM_MISSING_PROG(ACLOCAL, aclocal, $missing_dir)
          AM_MISSING_PROG(AUTOCONF, autoconf, $missing_dir)
          AM_MISSING_PROG(AUTOMAKE, automake, $missing_dir)
          AM_MISSING_PROG(AUTOHEADER, autoheader, $missing_dir)
          AM_MISSING_PROG(MAKEINFO, makeinfo, $missing_dir)

   그 외에 Automake가 필요로 하지만, `AM_INIT_AUTOMAKE'에 의해 실행되지
않는 매크로가 여기 있다:

`AC_OUTPUT'
     Automake는 만들어질 파일들을 결정하는 데 이 매크로를 사용한다.
     `Makefile'이라는 이름으로 열거된 파일들은 `Makefile'로 다뤄진다.
     그 외에 열거된 파일들은 다른 식으로 처리된다.  현재 다른 점은
     `Makefile'들이 `make distclean'에 의해 지워지지만, 다른 파일들은
     `make clean'으로 지워진다는 것 뿐이다.


File: automake-ko.info,  Node: 옵션,  Next: aclocal 실행,  Prev: 필요한 것,  Up: configure

그외 Automake가 이해하는 것
===========================

   또한 Automake는 특정 매크로를 알아내서 적절히 만들어지는
`Makefile.in'을 손본다.  현재 Automake가 알아내는 매크로와 그 효과는:

`AC_CONFIG_HEADER'
     Automake는 config header를 자동으로 다시 만들어 내는 rule을 만들어
     낸다.  이 매크로를 사용하면, `stamp-h.in' 파일을 소스 디렉토리에
     만들어야 한다.  이 파일은 빈 파일일 수 있다.  또 `configure.in'의
     `AC_OUTPUT' 명령은 `stamp-h'를 만들어야 한다.  예를 들어:
          AC_OUTPUT(Makefile,
          [test -z "$CONFIG_HEADERS" || echo timestamp > stamp-h])
     현재는 Automake가 `AC_OUTPUT' 명령이 올바른지 검사하지 않는 것에
     유의하자.  바라건데 앞으로 `autoconf' 버전은 Automake가 이 일을
     자동으로 처리하게 해 줄 것이다.

`AC_CONFIG_AUX_DIR'
     Automake는 `mkinstalldirs'와 같은 여러가지 도움이 되는 스크립트를
     이 매크로가 불리워진 디렉토리에서 찾을 것이다.  만약 찾을 수
     없으면, 스크립트를 그 "표준" 위치(맨 위 소스 디렉토리나, 또는 현재
     `Makefile.am'에 해당하는 소스 디렉토리 중에 적당한 곳)에서 찾는다.
     FIXME: give complete list of things looked for in this directory

`AC_PATH_XTRA'
     Automake는 `AC_PATH_XTRA'가 정의한 변수에 대한 정의문을 C
     프로그램이나 라이브러리를 build하는 각 `Makefile.in'에 넣는다.

`AC_CANONICAL_HOST'

`AC_CHECK_TOOL'
     Automake는 `config.guess'와 `config.sub'이 있는지 확인할 것이다.
     또, `Makefile' 변수 `host_alias'와 `host_triplet'이 사용될 것이다.

`AC_CANONICAL_SYSTEM'
     이것ㅇ은 `AC_CANONICAL_HOST'와 비슷하지만, `Makefile' 변수
     `build_alias'와 `target_alias'를 정의한다.

`AC_FUNC_ALLOCA'

`AC_FUNC_GETLOADAVG'

`AC_FUNC_MEMCMP'

`AC_STRUCT_ST_BLOCKS'

`AM_FUNC_FNMATCH'

`AM_FUNC_STRTOD'

`AC_REPLACE_FUNCS'

`AC_REPLACE_GNU_GETOPT'

`AM_WITH_REGEX'
     Automake는 적당한 소스 파일이 배포본의 일부인지를 확인할 것이다.
     그리고 이 object에 대해서 적당한 의존성이 만들어졌는지 확인할
     것이다.  더 많은 정보를 얻으려면 *Note 라이브러리::.

`LIBOBJS'
     Automake는 `.o' 파일들을 `LIBOBJS'로 넣는 문장을 찾아내고, 이
     부가적인 파일들을 마치 `AC_REPLACE_FUNC'를 통해 발견된 것처럼 다룰
     것이다.

`AC_PROG_RANLIB'
     패키지 안에서 라이브러리가 하나라도 build되면 이 매크로가 필요하다.

`AC_PROG_CXX'
     C++ 소스가 하나라도 포함되어 있으면 이 매크로가 필요하다.

`AM_PROG_LIBTOOL'
     Automake는 `libtool'을 위한 처리를 할 것이다.  (*note The Libtool
     Manual: (libtool)Top.).

`AC_PROG_YACC'
     Yacc 소스 파일이 보이면 이 매크로를 사용하거나, `configure.in'에
     `YACC' 변수를 정의해야 한다.  앞의 방법이 더 좋다.

`AC_DECL_YYTEXT'
     패키지에 Lex 소스가 있으면 이 매크로가 필요하다.

`AC_PROG_LEX'
     Lex 소스 파일이 있으면 이 캐르로를 사용해야 한다.

`ALL_LINGUAS'
     Automake가 `configure.in' 안에 이 변수가 정의된 것을 보면,
     Automake는 `po' 디렉토리를 검사해서 여기서 언급된 모든 `.po'
     파일들이 있는지 확인한다.  그리고 모든 `.po' 파일이 언급되었는지
     확인한다.

`AM_C_PROTOTYPES'
     자동으로 ANSI C 문법을 없애는 기능을 사용하려면, 이 매크로가
     필요하다.  *Note ANSI::를 보라.

`AM_GNU_GETTEXT'
     GNU gettext(*note gettext::)를 사용하는 패키지에 이 매크로가
     필요하다.  이 매크로는 gettext와 함께 배포된다.  Automake가 이
     매크로를 보면, 패키지가 gettext에서 필요한 것들을 만족하는지
     확인한다.

`AM_MAINTAINER_MODE'
     이 매크로는 `configure'에 `--enable-maintainer-mode' 옵션을
     붙인다.  이 매크로가 쓰이면, `automake'는 만들어 지는
     `Makefile.in'에서 기본으로 "maintainer만 사용하는" rule 들을
     없애도록 한다.  이 매크로는 `Gnits' 모드에서는 사용할 수 없다.
     FIXME xref.

`AC_SUBST'

`AC_CHECK_TOOL'

`AC_CHECK_PROG'

`AC_CHECK_PROGS'

`AC_PATH_PROG'

`AC_PATH_PROGS'
     각각의 매크로에 대하여 첫번째 arugment는 자동으로 각각의 만들어지는
     `Makefile.in'의 변수로 정의도니다.


File: automake-ko.info,  Node: aclocal 실행,  Next: 매크로,  Prev: 옵션,  Up: configure

aclocal.m4의 자동 생성
======================

   Automake는 패키지 내에서 쓰일 여러개의 Autoconf 매크로를 포함한다;
몇개는 실제로 특정 상황에서만 필요하다.  이러한 매크로는 `aclocal.m4'에
정의되야 한다; 그렇지 않으면, `autoconf'가 찾아내지 못할 것이다.

   `aclocal' 프로그램은 자동으로 `aclocal.m4' 파일을 `configure.in'
내용에 기초해서 만든다.  이 프로그램은 여기저기 돌아다닐 필요없이
Automake가 제공하는 매크로를 사용을 편리하게 해 준다.  또, 이러한
`aclocal'의 방법은 또다른 패키지에서 사용할때 확장할 수 있다.

   `aclocal'이 시작할때, 매크로 정의를 찾아 내기 위해서, 찾을 수 있는
모든 `.m4' 파일을 검색한다.  그 다음에 `configure.in'을 검색한다.
첫번째 단계에서 발견되는 매크로중 하나라도 언급되어 있으면 그 매크로와
그 매크로에서 필요한 다른 매크로들을 `aclocal.m4'에 집어 넣는다.

   `aclocal'에 다음 옵션을 줄 수 있다:

`--acdir=DIR'
     설치 디렉토리 대신에 DIR에서 매크로 파일들을 찾는다.  보통 이
     옵션은 디버깅을 위해 사용된다.

`--help'
     명령 행 옵션의 요약을 표시하고 종료한다.

`--output=FILE'
     출력을 `aclocal.m4' 대신에 FILE로 쓴다.

`--verbose'
     검사하고 있는 파일의 이름들을 출력한다.

`--version'
     Automake의 버전 번호를 표시하고 종료한다.


File: automake-ko.info,  Node: 매크로,  Next: aclocal 확장,  Prev: aclocal 실행,  Up: configure

Automake가 제공하는 Autoconf 매크로
===================================

`AM_CONFIG_HEADER'
     Automake는 컨피그 헤더(config header) 파일을 자동으로 다시 만드는
     룰(rule)을 만들어 낼 것이다.  이 매크로를 사용하면, `stamp-h.in'
     파일을 소스 디렉토리에 만들어야 한다.  이 파일은 텅 빈 파일일 수
     있다.

`AM_CYGWIN32'
     `configure'가 `Cygwin32' 환경에서 동작하는지 체크한다.  (FIXME
     xref).  만약 그렇다면, 변수 `EXEEXT'를 `.exe'로 정의한다; 그렇지
     않으면 이 변수를 빈 문자열로 한다.  Automake는 이 매크로를
     발견하면, `Cygwin32'에서 자동으로 동작하는 `Makefile.in'을
     만들도록 할 것이다.  `Cygwin32' 환경에서, `gcc'는 파일 이름이
     `.exe'로 끝나는 실행 파일을 만든다 (비록 명령행에서 `.exe'를
     명시할 필요는 없지만).  Automake는 이것을 멋지게 처리하는 특별한
     코드를 `Makefile.in'에 추가한다.

`AM_FUNC_STRTOD'
     `strtod' 함수를 사용할 수 없거나, 올바르게 동작하지 않으면 (SunOS
     5.4의 함수처럼), `strtod.o'를 `LIBOBJS' 변수에 출력한다.

`AM_FUNC_ERROR_AT_LINE'
     `error_at_line' 함수가 없으면, `LIBOBJS'에 `error.o'를 추가한다.

`AM_FUNC_MKTIME'
     `mktime' 함수가 동작하는지 확인한다.  만약 없으면, `mktime.o'를
     `LIBOBJS'에 추가한다.

`AM_FUNC_OBSTACK'
     GNU obstacks 가 있는지 확인한다; 없으면, `obstack.o'를 `LIBOBJS'에
     추가한다.

`AM_C_PROTOTYPES'
     함수 원형(prototype)이 컴파일러가 알아듣는지 확인한다.  만약
     그렇다면, `PROTOTYPES'를 정의하고, 변수 `U'와 `ANSI2KNR'을 빈
     문자열로 정의한다.  그렇지 않으면, `U'를 `_'로 정의하고,
     `ANSI2KNR'을 `./ansi2knr'로 정의한다.  Automake는 이 값들을
     자동으로 ANSI C 기능을 없애는 데 사용한다.

`AM_HEADER_TIOCGWINSZ_NEEDS_SYS_IOCTL'
     `TIOCGWINSZ'를 쓸때 `<sys/ioctl.h>'가 필요하면,
     `GWINSZ_IN_SYS_IOCTL'을 정의한다.  아니면, `TIOCGWINSZ'는
     `<termios.h>'에 있을 수도 있다.

`AM_INIT_AUTOMAKE'
     대부분의 `configure.in'에서 필요한 많은 매크로들을 실행한다.  이
     매크로는 두개의 인수로 패키지와 버전 번호가 필요하다.  기본적으로,
     이 매크로는 `PACKAGE'와 `VERSION'을 `AC_DEFINE'한다.  이 기능은
     세번째 인수로 무언가를 넘겨주면 피할 수 있다.

`AM_PATH_LISPDIR'
     `emacs' 프로그램을 찾는다.  그리고 찾으면, 출력 변수 `lispdir'을
     Emacs의 site-lisp 디렉토리의 경로(full path)로 맞춘다.

`AM_PROG_CC_STDC'
     만약 C 컴파일러가 기본적으로 ANSI C 모드가 아니면, 옵션을 추가해서
     `CC'가 ANSI C 모드로 동작하도록 시도한다.  이 매크로는 여러가지
     시스템에서 ANSI C를 선택하는 옵션들을 시도해 본다.  만약
     `__STDC__'가 1이고, 함수 원형(prototype)을 제대로 처리하면 ANSI C
     모드라고 생각한다.

     이 매크로를 쓰면, 매크로가 실행된 뒤에 C 컴파일러가 ANSI C를
     받아들이도록 맞춰졌는지 확인해야 한다; 그렇지 않으면, 셸 변수
     `am_cv_prog_cc_stdc'가 `no'로 정의된다.  소스 코드가 ANSI C로 되어
     있으면, `ansi2knr' 옵션을 써서 ANSI 기능을 없앤 카피를 만들 수
     있다.

`AM_PROG_INSTALL'
     `AC_PROG_INSTALL'과 마찬가지이지만, `INSTALL_SCRIPT'도 정의한다.

`AM_SANITY_CHECK'
     이 매크로는 빌드(build) 디렉토리에서 생성되는 파일이 소스
     디렉토리의 파일보다 최근에 만들어졌는지 확인한다.  이 기능은
     시계가 잘못 맞춰진 시스템에서는 실패한다.  이 매크로는
     `AM_INIT_AUTOMAKE'에서 자동으로 실행된다.

`AM_SYS_POSIX_TERMIOS'
     시스템에서 POSIX termios 헤더와 함수가 사용 가능한지 검사한다.
     가능하다면, `am_cv_sys_posix_termios' 셸 변수를 `yes'로 정의한다.
     그렇지 않으면, 변수를 `no'로 정의한다.

`AM_TYPE_PTRDIFF_T'
     `ptrdiff_t'가 `<stddef.h>'에 정의되어 있으면 `HAVE_PTRDIFF_T'를
     정의한다.

`AM_WITH_DMALLOC'
     `dmalloc' 패키지 지원을 추가한다.  사용자가 `--with-dmalloc'
     옵션을 붙여서 configure를 하면, `WITH_DMALLOC'을 정의하고,
     `LIBS'에 `-ldmalloc'을 추가한다.  `dmalloc' 패키지는
     <ftp://ftp.letters.com/src/dmalloc/dmalloc.tar.gz>에 있다.

`AM_WITH_REGEX'
     `configure' 명령행에 `--with-regex'을 추가한다.  만약 이 매크로가
     사용된다면 (사용되는 것이 기본), `regex' 정규식(regular
     expression) 라이브러리가 사용되고, `regex.o'가 `LIBOBJS'에
     추가되고, `WITH_REGEX'가 정의된다.  `--without-regex' 옵션이
     있다면, `rx' 정규식(regular expression) 라이브러리가 사용되고,
     `rx.o'가 `LIBOBJS'에 추가된다.


File: automake-ko.info,  Node: aclocal 확장,  Prev: 매크로,  Up: configure

자기만의 aclocal 매크로 작성하기
================================

   Aclocal은 내부적으로 어떤 매크로에 대해서도 알지 못하기 때문에,
자기만의 매크로를 가지고 확장하는 것은 쉬운 일이다.

   확장하는 것은 다른 프로그램에서 사용할때 쓰이는 고유한 Autoconf
매크로를 포함하는 라이브러리가 사용한다.  예를 들어, `gettext'
라이브러리는 `gettext'를 쓰는 어떤 패키지든지 사용해야 하는
`AM_GNU_GETTEXT'매크로를 포함한다.  라이브러리가 설치되면, 라이브러리는
`aclocal'이 찾을 수 있도록 이 매크로를 설치한다.

   매크로 파일은 `AC_DEFUN'의 연속이어야 한다.  또 `aclocal'은
`AC_REQUIRE'를 알아보기 때문에, 각각의 매크로를 다른 파일에 넣는 것도
가능하다.

   매크로 파일의 이름은 `.m4'로 끝나야 한다.  이러한 파일은
`$(datadir)/aclocal'에 설치되어야 한다.


File: automake-ko.info,  Node: 최상위 레벨,  Next: 프로그램과 라이브러리,  Prev: configure,  Up: Top

최상위 레벨의 `Makefile.am'
***************************

   flat이 아닌 패키지에서는, 맨 위의 `Makefile.am'은 Automake에게 어떤
서브디렉토리가 build되어야 하는지 알려줘야 한다.  `SUBDIRS' 변수를
통해서 알려준다.

   `SUBDIRS' 매크로는 여러가지 종류의 building이 일어나야 하는
서브디렉토리들의 나열을 담고 있다.  만들어질 `Makefile'의 많은
target들(예로 `all')은 현재 디렉토리와 언급된 모든 서브디렉토리에서
실행될 것이다.  `SUBDIRS'에 열거된 디렉토리에 `Makefile.am'이 들어 있을
필요가 없다는 것에 유의하자; (configuration뒤에) 오직 `Makefile'만
필요하다.  이 특징은 Automake를 사용하지 않는 패키지(`gettext'와
같은)에서 라이브러리를 포함할 때 유용하다.  `SUBDIRS'에 언급된
디렉토리들은 현재 디렉토리의 바로 밑 디렉토리여야 한다.  예를 들어서,
`src/subdir'를 `SUBDIRS'에 쓸 수 없다.

   딥(deep) 패키지에서, 맨 위의 `Makefile.am'은 아주 짧기도 하다.  예를
들어 여기 Hello 배포판의 `Makefile.am'이 있다.

     EXTRA_DIST = BUGS ChangeLog.O README-alpha
     SUBDIRS = doc intl po src tests

   `SUBDIRS'는 configure의 치환(substitution) (예를 들어 `@DIRS@')를
포함할 수 있다; Automake 자신은 실제로 이 변수의 내용을 검사하지 않는다.

   `SUBDIRS'가 정의되면, `configure.in'은 `AC_PROG_MAKE_SET'을 포함해야
한다.

   `SUBDIRS'는 맨 위 디렉토리의 `Makefile.am'에만 사용할 수 있는 것은
아니다.  Automake는 임의의 깊이의 패키지를 만드는데 사용할 수 있다.


File: automake-ko.info,  Node: 프로그램과 라이브러리,  Next: 그외 object,  Prev: 최상위 레벨,  Up: Top

프로그램과 라이브러리 build하기
*******************************

   Automake 기능의 많은 부분은 C 프로그램과 라이브러리를 쉽게 만들 수
있도록 하기 위한 것이다.

* Menu:

* 프로그램::
* 라이브러리::
* LIBOBJS::
* 동적 라이브러리::
* 프로그램 변수들::
* Yacc와 Lex::
* C++::
* ANSI::
* 의존성::


File: automake-ko.info,  Node: 프로그램,  Next: 라이브러리,  Prev: 프로그램과 라이브러리,  Up: 프로그램과 라이브러리

프로그램을 build하기
====================

   (라이브러리에 대조되어) 프로그램으로 빌드되는 소스가 들어 있는
디렉토리에서, `PROGRAMS' primary가 사용된다.  프로그램은 `bindir',
`sbindir', `libexecdir', `pkglibdir'에 설치되거나, 아예 설치되지 않을
수도 (`noinst') 있다.

   예를 들어:

     bin_PROGRAMS = hello

   위의 간단한 경우에서, `Makefile.in'은 결과적으로 `hello'라는 이름의
프로그램을 만드는 코드를 갖게 될 것이다.  `hello_SOURCES' 변수는 어떤
소스 파일이 실행 파일로 build될 것인지 알리는 데 쓰인다.

     hello_SOURCES = hello.c version.c getopt.c getopt1.c getopt.h system.h

   위는 각 `.c' 파일이 대응되는 `.o' 파일로 컴파일되고, 그 다음에
링크되어 `hello'를 만들도록 한다.

   `prog_SOURCES'가 필요한데 언급되지 않은 경우, 기본은 한개의 `prog.c'
파일이다.  위의 예에서, `hello_SOURCES'의 정의는 실제로는 필요없는
것이다.

   한개의 디렉토리에서 여러개의 프로그램이 build될 수 있다.  여러개의
프로그램은 한개의 소스 파일을 공유할 수 있다.  그 소스 파일은 각각의
`_SOURCES' 정의에 열거되어야 한다.

   `_SOURCES' 정의에 열거된 헤더 파일들은 배포본에 포함될 것이지만,
한편 무시될 것이다.  명백하지 않은 경우, `_SOURCES' 변수에
`configure'에 의해 만들어지는 헤더 파일을 포함해서는 안된다; 이 파일을
배포되는 것이 아니다.  Lex(`.l')와 Yacc(`.y') 파일들 또한 열거될 수
있다; *Note Yacc와 Lex::를 보라.

   모든 상황에서 모든 파일이 빌드(build)되는 것이 아닐지라도, Automake는
프로그램에 들어갈 가능성이 있는 소스 파일으 ㄹ모두 알고 있어야 한다.
조건에 따라 빌드(build)되는 파일은 적절한 `EXTRA_'변수에 열거되야 한다.
예를 들어, `hello-linux.c'가 조건에 따라서 `hello'에 포함된다면,
`Makefile.am'은 다음을 포함해야 한다:

     EXTRA_hello_SOURCES = hello-linux.c

   비슷하게, 때로는 configure시에 무엇이 빌드(build)될지 결정하는 것이
좋다.  예를 들어, GNU `cpio'는 특정 상황에서만 `mt'와 `rmt'를
빌드(build)한다.

   이런 경우, `automake'는 build될 가능성이 있는 프로그램을 모두
알려줘야 한다.  하지만, 동시에 만들어질 `Makefile.in'은 `configure'가
명시하는 프로그램을 사용하도록 해야 한다.  이 일은 옵션으로 build되는
프로그램을 `EXTRA_PROGRAMS'에 적어주는 한편, 각 `_PROGRAMS' 정의에
`configure' 치환(substitute) 값을 적어주면 된다.

   `configure'가 직접 찾지 않는 라이브러리와 링크해야 한다면, `LDADD'를
쓸 수 있다.  이 변수는 링커의 명령행에 어떤 옵션이라도 첨가하고 싶을때
사용할 수 있다.

   가끔, 여러개의 프로그램이 한개의 디렉토리에서 build되면서 링크시에
필요한 것이 같지 않을 수 있다.  이 경우, global `LDADD'를 재정의하기
위해 `PROG_LDADD' (PROG는 `_PROGRAMS' 변수 안에 나타나는 프로그램의
이름이고, 보통 소문자로만 쓰여진다) 변수를 사용한다.  (만약 이 변수가
어떤 프로그램에 주어졌다면 그 프로그램은 `LDADD'를 써서 링크되지
않는다.)

   예를 들어, GNU cpio에서, `pax', `cpio', 그리고 `mt'는 `libcpio.a'
라이브러리와 링크된다.  하지만, `rmt'는 같은 디렉토리에서 build되면서,
링크할때 그런 것이 필요없다.  또 `mt'와 `rmt'는 특정 architecture에서만
build된다.  여기 cpio의 `src/Makefile.am'이 있다 (요약):

     bin_PROGRAMS = cpio pax @MT@
     libexec_PROGRAMS = @RMT@
     EXTRA_PROGRAMS = mt rmt
     
     LDADD = ../lib/libcpio.a @INTLLIBS@
     rmt_LDADD =
     
     cpio_SOURCES = ...
     pax_SOURCES = ...
     mt_SOURCES = ...
     rmt_SOURCES = ...

   `prog_LDADD'는 프로그램에 관계된 링커 옵션(`-l'과 `-L'을 제외하고)을
넘겨주는 데는 부적합하다.  그래서 이런 목적으로는 `prog_LDFLAGS'를
사용한다.

   때때로 그 프로그램의 일부가 아닌 어떤 다른 target에 의존하는
프로그램이 유용할 수 있다.  이렇게 하려면 `prog_DEPENDENCIES' 변수를
쓰면 된다.  각 프로그램은 이 변수의 내용에 의존하게 되지만, 그 이상의
해석은 하지 않는다.

   `prog_DEPENDENCIES'가 제공되지 않으면, 이것은 Automake에 의해
결정된다.  자동으로 주어지는 값은 `prog_LDADD'의 내용에서 대부분의
configure 치환(substitution), `-l', 그리고 `-L' 옵션을 뺀 것이다.  남아
있게 되는 configure 치환(substitution)은 `@LIBOBJS@'와
`@ALLOCA@'뿐이다; 이 옵션들은 만들어지게 되는 `prog_DEPENDENCIES'에
잘못된 값이 들어가게 하지 않는다고 알려져 있기 때문에 남아 있는다.


File: automake-ko.info,  Node: 라이브러리,  Next: LIBOBJS,  Prev: 프로그램,  Up: 프로그램과 라이브러리

라이브러리를 build하기
======================

   라이브러리를 build하는 것은 프로그램을 build하는 것과 많이 비슷하다.
이 경우, primary의 이름은 `LIBRARIES'이다.  라이브러리는 `libdir'나
`pkglibdir'에 설치될 수 있다.

   각 `_LIBRARIES' 변수는 build될 라이브러리를 열거한 것이다.  예를
들어 `libcpio.a'라는 이름의 라이브러리를 만들고, 설치하지 않으려면,
다음과 같이 쓴다:

     noinst_LIBRARIES = libcpio.a

   라이브러리로 들어가게 되는 소스는 프로그램에서 하는 것과 마찬가지로
`_SOURCES' 변수를 통해 결정된다.  라이브러리의 이름은 규범화된다는
(*note 규범화::) 것에 유의하자.  그래서 `liblob.a'에 대한 `_SOURCES'
변수는 `liblob_a_SOURCES'이지 `liblob.a_SOURCES'가 아니다.

   `library_LIBADD' 변수를 사용해서 추가적으로 라이브러리에 object를
첨가할 수 있따.  이 변수는 `configure'에 의해 결정되는 object를 위해
사용된다.  다시 cpio의 경우:

     libcpio_a_LIBADD = @LIBOBJS@ @ALLOCA@


File: automake-ko.info,  Node: LIBOBJS,  Next: 동적 라이브러리,  Prev: 라이브러리,  Up: 프로그램과 라이브러리

LIBOBJS and ALLOCA의 특별 처리
==============================

   Automake는 자동으로 배포판에 (*note 배포::) 소스 파일을 포함하기 위해
`@LIBOBJS@'와 `@ALLOCA@'의 사용을 외포적으로 알아내서, 그 정보와
`configure.in'에서 나온 `LIBOBJS' 파일의 목록을 활용한다.  이 소스
파일들은 자동으로 의존성 추적 방식에 제어될 것이다.  *Note 의존성::.

   `@LIBOBJS@'와 `@ALLOCA@'는 `_LDADD'나 `_LIBADD' 변수에게 특별히
인식될 것이다.


File: automake-ko.info,  Node: 동적 라이브러리,  Next: 프로그램 변수들,  Prev: LIBOBJS,  Up: 프로그램과 라이브러리

동적 라이브러리(shared library) 만들기
======================================

   동적 라이브러리(shared library)를 만드는 것은 비교적 복잡한 일이다.
이런 이유로, GNU Libtool (*note The Libtool Manual: (libtool)Top.)이
플랫폼(platform)에 관계없이 동적 라이브러리(shared library)를 만들기
위해 작성되었다.

   Automake는 `LTLIBRARIES' 주요변수를 정의해서 라이브러리를 만드는 데
Libtool을 쓴다.  각 `_LTLIBRARIES' 변수는 만들어야 할 동적
라이브러리(shared library)의 리스트이다.  예를 들어, `libgettext.a'와
이에 상대되는 동적 라이브러리(shared library)를 만들고, `libdir'에
설치하려면, 다음과 같이 쓴다:

     lib_LTLIBRARIES = libgettext.la

   동적 라이브러리(shared library)는 반드시 설치되야 하므로,
`noinst_LTLIBRARIES'와 `check_LTLIBRARIES'변수는 사용할 수 없다.

   각 라이브러리에 대해서, `library_LIBADD' 변수는 동적 라이브러리에
추가할 추가적인 libtool 오브젝트(object)의 이름을 담고 있다.
`library_LDFLAGS' 변수는 `-version-info'나 `static'과 같은 그외 추가할
libtool 옵션을 담고 있다.

   같은 디렉토리에 설치할 라이브러리에 대해서는, `automake'는 자동으로
적절한 `-rpath' 옵션을 추가한다.  하지만, configure 시에 결정해야 할
(즉 `EXTRA_LTLIBRARIES'에 언급되는) 라이브러리에 대해서는, `automake'는
결과적으로 설치할 디렉토리에 대해서 알지 못한다; 그런 디렉토리에서는
`-rpath' 옵션을 해당되는 `_LDFLAGS' 변수에 직접 추가해야 한다.

   자세한 정보는 *Note Using Automake with Libtool: (libtool)Using
Automake.


File: automake-ko.info,  Node: 프로그램 변수들,  Next: Yacc와 Lex,  Prev: 동적 라이브러리,  Up: 프로그램과 라이브러리

프로그램을 build할때 쓰이는 변수들
==================================

   때로는 어떤 `Makefile' 변수가 Automake가 컴파일을 위해 사용하는
것인지 아는 것이 유용하다; 예를 들어 어떤 특별한 경우에만 컴파일을 해야
할지도 모른다.

   어떤 변수는 Autoconf에서 물려받는 것이다; 그것들은 `CC', `CFLAGS',
`CPPFLAGS', `DEFS', `LDFLAGS', 그리고 `LIBS'이다.

   Automake가 직접 추가로 정의하는 몇개 변수가 있다.

`INCLUDES'
     `-I' 옵션들의 리스트이다.  이 변수는 특별히 들여다보고 싶은
     디렉토리가 있으면 `Makefile.am'에 지정할 수 있다.  `automake'는
     이미 자동으로 몇개의 `-I' 옵션을 추가한다.  특히 `automake'는
     `-I$(srcdir)'와 `config.h'가 들어 있는 디렉토리를 가리키는 `-I'를
     만든다(만약 `AC_CONFIG_HEADER'나 `AM_CONFIG_HEADER'를 사용했다면).

     실제로는 `INCLUDES'는 `-I' 외에 다른 `cpp' 옵션을 쓰고 싶을때
     쓰인다.  예를 들어, 임의의 `-D' 옵션을 컴파일러에 넘겨주고 싶을 때
     쓰인다.

`COMPILE'
     이 변수는 C 소스 파일을 컴파일할때 실제로 사용될 명령이다.
     파일이름은 이 완전한 명령행 뒤에 붙여진다.

`LINK'
     이것은 실제 C 프로그램을 링크할때 쓰이는 명령이다.


File: automake-ko.info,  Node: Yacc와 Lex,  Next: C++,  Prev: 프로그램 변수들,  Up: 프로그램과 라이브러리

Yacc와 Lex 지원
===============

   Automake는 좀 특이하게 Yacc와 Lex를 지원한다.

   Automake는 yacc(또는 lex)에 의해 만들어진 `.c' 파일은 입력파일의
basename을 사용해서 이름지어진다고 가정한다.  즉, yacc 소스 파일
`foo.y'의 경우, automake는 중간 파일은 `foo.c'라는 이름(더 전통적인
이름인 `y.tab.c'가 아니라)으로 만들어 낸다.

   yacc 소스 파일의 확장자는 `C' 혹은 `C++' 결과 파일의 확장자를
결정하는 데 ㅆ인다.  `.y' 확장자를 가진 파일은 `.c' 파일이 된다;
마찬가지로, `samp.yy'는 `.cc'가 된다; `.y++'은 `c++'이 된다; `.yxx'는
`cxx'.  마찬가지로, lex 소스 파일은 `C' 혹은 `C++' 파일을 만드는 데
쓰인다; `.l', `.ll', `.l++', 그리고 `.lxx' 확장자를 알아본다.

   어떤 `SOURCES' 변수에도 중간 파일(`C' 혹은 `C++')을 언급하면 안
된다; 오직 소스 파일만을 열거한다.

   yacc(혹은 lex)가 만드는 중간 파일은 배포본을 만들때 포함된다.
그래서, 사용자는 yacc나 lex를 갖고 있을 필요가 없다.

   yacc 소스 파일이 있으면, `configure.in'은 `YACC' 변수를 정의해야
한다.  이 작업은 `AC_PROG_YACC' 매크로를 실행시켜서 쉽게 할 수 있다.

   비슷하게 lex 소스 파일이 있으면 `configure.in'은 `LEX' 변수를
정의해야 한다.  이것을 하기 위해 `AC_PROG_LEX'를 쓸 수 있다.
Automake의 lex 지원을 위해서는 `AC_DECL_YYTEXT' 매크로도 사용해야 한다
- automake는 `LEX_OUTPUT_ROOT'의 값을 알 필요가 있다.

   lex 소스 파일을 포함하는 어떤 프로그램이든 `@LEXLIB@'와 링크되야
한다.  이 작업은 적당한 `LDADD' 변수에 `@LEXLIB@'를 넣어서 할 수 있다.

   Automake는 여러개의 yacc (또는 lex) 소스 파일을 한개의 프로그램에
포함할 수 있게 해 준다.  Automake는 여러개의 yacc 실행간의 lock을
관리하기 위해 `interlock'이라는 작은 프로그램을 사용한다.  yacc의 출력
파일이름은 고정되어 있고, 병렬로 make를 실행할 경우 여러 instance의
`yacc'를 동시에 실행할 수 있기 때문에 이 기능이 필요하다.
`interlock'은 automake와 함께 배포된다.  이 프로그램은
`AC_CONFIG_AUX_DIR'에서 언급된 디렉토리에 있거나, 혹은 이 매크로가
`configure.in'에서 사용되지 않았다면 현재 디렉토리에 있어야 한다.

   `yacc'에서는, 단순한 locking 관리는 충분하지 않다.  `yacc' 출력은
내부적으로 항상 같은 symbol을 사용할 것이고, 두개의 `yacc' parser를
동일한 실행파일로 링크할 수 없다.

   `gdb'에서 사용하고 있는 방법인 이름을 고치는 해킹(hack)을 추천한다:
     #define	yymaxdepth c_maxdepth
     #define	yyparse	c_parse
     #define	yylex	c_lex
     #define	yyerror	c_error
     #define	yylval	c_lval
     #define	yychar	c_char
     #define	yydebug	c_debug
     #define	yypact	c_pact
     #define	yyr1	c_r1
     #define	yyr2	c_r2
     #define	yydef	c_def
     #define	yychk	c_chk
     #define	yypgo	c_pgo
     #define	yyact	c_act
     #define	yyexca	c_exca
     #define yyerrflag c_errflag
     #define yynerrs	c_nerrs
     #define	yyps	c_ps
     #define	yypv	c_pv
     #define	yys	c_s
     #define	yy_yys	c_yys
     #define	yystate	c_state
     #define	yytmp	c_tmp
     #define	yyv	c_v
     #define	yy_yyv	c_yyv
     #define	yyval	c_val
     #define	yylloc	c_lloc
     #define yyreds	c_reds
     #define yytoks	c_toks
     #define yylhs	c_yylhs
     #define yylen	c_yylen
     #define yydefred c_yydefred
     #define yydgoto	c_yydgoto
     #define yysindex c_yysindex
     #define yyrindex c_yyrindex
     #define yygindex c_yygindex
     #define yytable	 c_yytable
     #define yycheck	 c_yycheck

   각 define에서 `c_' 접두어를 좋은대로 바꾸라.  이 define은 `bison',
`byacc', 그리고 전통적인 `yacc' 버전에서 동작한다.  만약 여기 언급되지
않은 symbol을 사용하는 parser generator를 찾으면, 새로운 이름을
알려주면 그것은 리스트에 추가될 것이다.


File: automake-ko.info,  Node: C++,  Next: ANSI,  Prev: Yacc와 Lex,  Up: 프로그램과 라이브러리

C++과 그외 언어들
=================

   Automake는 C++에 대해서 완전히 지원하고, 다른 언어들에 대해서
기본적인 지원을 한다.  다른 언어들에 대한 지원은 요구에 따라서 향상될
것이다.

   C++ 코드를 포함한 패키지는 `CXX' 변수를 `configure.in'에서 정의해야
한다; 이 작업을 하기 위한 가장 간단한 방법은 `AC_PROG_CXX' 매크로를
사용하는 것이다.

   C++ 소스 파일이 있을때 몇개 변수가 추가로 정의된다.

`CXX'
     C++ 컴파일러의 이름.

`CXXFLAGS'
     C++ 컴파일러에 붙을 flag.

`CXXCOMPILE'
     명령은 실제로 C++ 소스 파일을 컴파일할 때 쓰인다.  완전한 명령 행을
     만들기 위해 파일 이름이 뒤에 붙여진다.

`CXXLINK'
     이 명령은 실제로 C++ 프로그램을 링크할 때 쓰인다.

